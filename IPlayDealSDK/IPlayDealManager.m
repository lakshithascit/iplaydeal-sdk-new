//
//  IPlayDealManager.m
//  IPlayDealSDK
//
//  Created by Saman Kumara on 7/5/16.
//  Copyright © 2016 iplayDeal. All rights reserved.
//

#import "IPlayDealManager.h"
#import "IPDLoginAreaViewController.h"
#import "AppUserDefaults.h"
#import "Reachability.h"
#import "Constants.h"



@interface IPlayDealManager (){
    BOOL isInSyncProcess;
}
@property (nonnull, strong) IPDLoginAreaViewController   *mainLoginViewController;
@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;

@end

@implementation IPlayDealManager

+ (id)sharedManagerWithAppKey:(NSString *)key secret:(NSString *)secret appVersion:(NSString *)version {
    static IPlayDealManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager           = [[self alloc] init];
        if (key) {
            sharedManager.appKey    = key;
            sharedManager.appSecret = secret;
            sharedManager.appVersion= version;
        }
    });
    return sharedManager;
}

+(id)sharedManager{
    return [IPlayDealManager sharedManagerWithAppKey:nil secret:nil appVersion:nil];
}
-(void)showLoginDialog:(UIViewController *)viewController{
    if (![AppUserDefaults getIsLogedIn]) {
        NSString* const frameworkBundleID  = SDKBUNDLE_ID;
        NSBundle* bundle = [NSBundle bundleWithIdentifier:frameworkBundleID];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"IPDStoryBoard" bundle:bundle];
        self.mainLoginViewController = [storyboard instantiateViewControllerWithIdentifier:@"IPDLoginAreaViewController"];
        [viewController presentViewController:self.mainLoginViewController animated:YES completion:^{}];
    }
}
#pragma mark -
#pragma mark Online
-(void)signout{
    [AppUserDefaults removeLogin];
}

- (BOOL)isLoggedIn {
    BOOL isLoggedIn = [AppUserDefaults getIsLogedIn];
    return isLoggedIn;
}
-(void)sendDetails:(NSString *)params{
    if ([self isLoggedIn] && [self connected]) {
        [[IPDWebserviceApi sharedManager] setDelegate:self];
        [[IPDWebserviceApi sharedManager] sendDetails:params];
    }else{
        [self storeData:params];
    }
}
-(void)storeData :(NSString *)dataParams{
    NSMutableArray * offlineData = [[NSMutableArray alloc]initWithArray:[AppUserDefaults getOfflineArray]];
    [offlineData addObject:dataParams];
    [AppUserDefaults setOfflineArray:offlineData];
    [self startOfflineWatching];
}
-(void)didReceiveSyncDataInfo:(id)respond Status:(BOOL)isSuccess{
    if (isSuccess) {
//        NSLog(@"Data send Success ONLINE");
    }else{
//        NSLog(@"Data send fail %@",respond);
    }
}

#pragma mark -
#pragma mark Offline
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)startOfflineWatching{
  
    NSString *remoteHostName = CONNECTION_SERVER;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
    [self.hostReachability startNotifier];
}
- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self syncofflineUpdates:curReach];
}
-(void)syncofflineUpdates :(Reachability *)reachability{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    if (netStatus != NotReachable){
        if ([[AppUserDefaults getOfflineArray] count]>0) {
            NSArray * offlineDataSet = [AppUserDefaults getOfflineArray];
            NSDictionary * bulkDic = @{@"bulk" : offlineDataSet};
            NSString * jsonString = [self bv_jsonStringWithPrettyPrint:bulkDic];
            [self sendOfflineDetails:jsonString];
        }
    }
}
-(void)sendOfflineDetails:(NSString *)params{
    if ([self isLoggedIn]&&!isInSyncProcess) {
        isInSyncProcess = YES;
        [[IPDWebserviceApi sharedManager] setDelegate:self];
        [[IPDWebserviceApi sharedManager] sendOfflineDetails:params];
    }
}
-(void)didReceiveOfflineSyncDataInfo:(id)respond Status:(BOOL)isSuccess{
    isInSyncProcess = NO;
    if (isSuccess)[AppUserDefaults removeStoredData];
}
#pragma mark -
#pragma mark Create Json
-(NSString*) bv_jsonStringWithPrettyPrint:(NSDictionary *) dictionaryData {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionaryData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
@end