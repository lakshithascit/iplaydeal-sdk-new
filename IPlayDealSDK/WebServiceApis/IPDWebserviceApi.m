//
//  IPDWebserviceApi.m
//  TestSDK
//
//  Created by SCIT on 8/24/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import "IPDWebserviceApi.h"
#import "IPlayDealManager.h"
#import "Constants.h"
#import "AppUserDefaults.h"
#import "FTHTTPCodes.h"

@implementation IPDWebserviceApi

+ (id)sharedManager {
    static IPDWebserviceApi *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(NSMutableURLRequest *)getRequestFromURL:(NSString *)url {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    if ([AppUserDefaults getIsLogedIn]) {
        NSLog(@"token -->> %@", [AppUserDefaults getAccessToken]);
        [request setValue:[AppUserDefaults getAccessToken] forHTTPHeaderField:@"oauth-token"];
        [request setValue:[AppUserDefaults getRefreshToken] forHTTPHeaderField:@"refresh-token"];
    }
    [request setHTTPMethod:@"POST"];
    return request;
}

-(void)Login :(NSString *)Username :(NSString *)password{
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession * session =  [NSURLSession sessionWithConfiguration:sessionConfig];
    NSString *URLString = [NSString stringWithFormat:@"%@signin",SERVICEURL];
    NSMutableURLRequest *request = [self getRequestFromURL:URLString];
    NSString *postString = [NSString stringWithFormat:@"{\"app_secret\" : \"%@\",\"app_key\" : \"%@\", \"app_version\": %d, \"user_name\":\"%@\", \"password\":\"%@\"}", [[IPlayDealManager sharedManager] appSecret], [[IPlayDealManager sharedManager] appKey], [[[IPlayDealManager sharedManager] appVersion] intValue],Username,password ];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"RequestSend %@",postString);
    
    NSURLSessionDataTask *s = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSError *jsonError;
        NSData *objectData = [newStr dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if ([[FTHTTPCodes descriptionForCode:[httpResponse statusCode]] isEqualToString:@"success"]) {
            NSLog(@"Request received %@",response);
            [self.delegate didReceiveLogininfo:json Status:YES];
        }else{
            [self.delegate didReceiveLogininfo:json Status:NO];
        }
    }];
    [s resume];
    
}
-(void)SignUp :(NSString *)firstName LatName:(NSString *)lastname Email:(NSString *)email Password:(NSString *)password{
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession * session =  [NSURLSession sessionWithConfiguration:sessionConfig];
    NSString *URLString = [NSString stringWithFormat:@"%@register",SERVICEURL];
    NSMutableURLRequest *request = [self getRequestFromURL:URLString];
    NSString *postString = [NSString stringWithFormat:@"{\"first_name\": \"%@\",\"last_name\":\"%@\", \"email\":\"%@\",    \"password\":\"%@\"\n}", firstName,lastname, email,password ];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask *s = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"---%@", newStr);
        
        NSError *jsonError;
        NSData *objectData = [newStr dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if ([[FTHTTPCodes descriptionForCode:[httpResponse statusCode]] isEqualToString:@"success"]) {
            [self.delegate didReceiveSignUpinfo:json Status:YES];
        }else{
            [self.delegate didReceiveSignUpinfo:json Status:NO];
        }
    }];
    [s resume];
}
-(void)sendDetails:(NSString *)params{
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession * session =  [NSURLSession sessionWithConfiguration:sessionConfig];
    NSString *URLString = [NSString stringWithFormat:@"%@/%@/parameters",SERVICEURL,[[IPlayDealManager sharedManager] appVersion]];
    NSMutableURLRequest *request = [self getRequestFromURL:URLString];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"Request %@",URLString);
    
    NSURLSessionDataTask *s = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
    NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if ([[FTHTTPCodes descriptionForCode:[httpResponse statusCode]] isEqualToString:@"success"]) {
            [self.delegate didReceiveSyncDataInfo:newStr Status:YES];
        }else{
            [self.delegate didReceiveSyncDataInfo:newStr Status:NO];
        }
    }];
    [s resume];
}
-(void)sendOfflineDetails:(NSString *)params{
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession * session =  [NSURLSession sessionWithConfiguration:sessionConfig];
    NSString *URLString = [NSString stringWithFormat:@"%@/%@/offline/parameters",SERVICEURL,[[IPlayDealManager sharedManager] appVersion]];
    NSMutableURLRequest *request = [self getRequestFromURL:URLString];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"Request %@ params %@",URLString,params);
    
    NSURLSessionDataTask *s = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if ([[FTHTTPCodes descriptionForCode:[httpResponse statusCode]] isEqualToString:@"success"]) {
            [self.delegate didReceiveOfflineSyncDataInfo:newStr Status:YES];
        }else{
            [self.delegate didReceiveOfflineSyncDataInfo:newStr Status:NO];
        }
    }];
    [s resume];
}

@end
