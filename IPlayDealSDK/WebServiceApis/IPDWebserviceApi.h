//
//  IPDWebserviceApi.h
//  TestSDK
//
//  Created by SCIT on 8/24/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IPDWebservicedelegate <NSObject>
@optional

-(void)didReceiveLogininfo :(id)respond Status:(BOOL)isSuccess;
-(void)didReceiveSignUpinfo :(id)respond Status:(BOOL)isSuccess;
-(void)didReceiveSyncDataInfo :(id)respond Status:(BOOL)isSuccess;
-(void)didReceiveOfflineSyncDataInfo :(id)respond Status:(BOOL)isSuccess;

@end

@interface IPDWebserviceApi : NSObject

@property ( nonatomic, retain ) id <IPDWebservicedelegate> delegate;

-(void)Login :(NSString *)Username :(NSString *)password;
-(void)SignUp :(NSString *)firstName LatName:(NSString *)lastname Email:(NSString *)email Password:(NSString *)password;
-(void)sendDetails:(NSString *)params;
-(void)sendOfflineDetails:(NSString *)params;

+ (id)sharedManager;

@end
