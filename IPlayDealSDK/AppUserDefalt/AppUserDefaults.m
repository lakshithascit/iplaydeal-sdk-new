//
//  AppUserDefaults.m
//  RsturantApp
//
//  Created by Lakshitha Benaragama on 2/22/13.
//  Copyright (c) 2013 JBMdigital(pvt)Ltd. All rights reserved.
//

#import "AppUserDefaults.h"
#import "Constants.h"
#define USER_DEFAULTS [NSUserDefaults standardUserDefaults]

@implementation AppUserDefaults


+ ( void ) setIsLogedIn:(BOOL) Record
{
    [USER_DEFAULTS setBool:Record forKey:@"ISLOGEDIN"];
    [USER_DEFAULTS synchronize];
}

+ ( BOOL ) getIsLogedIn
{
    return [USER_DEFAULTS objectForKey:@"ISLOGEDIN"];
}

+ ( void ) setImagePath:(NSString *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"IMAGEPATH"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getImagePath
{
    return [USER_DEFAULTS objectForKey:@"IMAGEPATH"];
}
+ ( void ) setAppVersion:(NSString *) Record{
    [USER_DEFAULTS setObject:Record forKey:@"APPVERSION"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getAppVersion
{
    return [USER_DEFAULTS objectForKey:@"APPVERSION"];
}
+ ( void ) setAccessToken:(NSString *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"APPACCESSTOKEN"];
    if ([USER_DEFAULTS synchronize])[self setIsLogedIn:YES];
}
+ ( NSString *) getAccessToken
{
    return [USER_DEFAULTS objectForKey:@"APPACCESSTOKEN"];
}
+ ( void ) setRefreshToken:(NSString *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"APPREFRESHTOKEN"];
    if ([USER_DEFAULTS synchronize])[self setIsLogedIn:YES];
}
+ ( NSString *) getRefreshToken
{
    return [USER_DEFAULTS objectForKey:@"APPREFRESHTOKEN"];
}

+ ( BOOL ) removeLogin
{
    [USER_DEFAULTS removeObjectForKey:@"APPTOKEN"];
    if ([USER_DEFAULTS synchronize])[self setIsLogedIn:NO];
    
    return [USER_DEFAULTS synchronize];
}
+ ( void ) setOfflineArray:(NSArray *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"OFFLINEDATAARRAY"];
    [USER_DEFAULTS synchronize];
}
+ ( NSArray *) getOfflineArray
{
    return [USER_DEFAULTS objectForKey:@"OFFLINEDATAARRAY"];
}
+ ( BOOL ) removeStoredData
{
    [USER_DEFAULTS removeObjectForKey:@"OFFLINEDATAARRAY"];
    return [USER_DEFAULTS synchronize];
}
@end
