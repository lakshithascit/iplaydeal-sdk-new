//
//  AppUserDefaults.h
//  RsturantApp
//
//  Created by Ganidu Ashen on 2/22/13.
//  Copyright (c) 2013 JBMdigital(pvt)Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUserDefaults : NSObject

+ ( BOOL ) getIsLogedIn;

+ ( void ) setImagePath:(NSString *) Record;
+ ( NSString *) getImagePath;

+ ( void ) setAppVersion:(NSString *) Record;
+ ( NSString *) getAppVersion;

+ ( void ) setAccessToken:(NSString *) Record;
+ ( NSString *) getAccessToken;

+ ( void ) setRefreshToken:(NSString *) Record;
+ ( NSString *) getRefreshToken;

+ ( BOOL ) removeLogin;

+ ( void ) setOfflineArray:(NSArray *) Record;
+ ( NSArray *) getOfflineArray;

+ ( BOOL ) removeStoredData;
@end
