//
//  IPDLoginAreaViewController.h
//  SDKSample
//
//  Created by Saman Kumara on 7/14/16.
//  Copyright © 2016 Saman Kumara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IPDWebserviceApi.h"

@interface IPDLoginAreaViewController : UITableViewController<IPDWebservicedelegate>{
    
    IPDWebserviceApi * webServiceApi;
    
}

@end
