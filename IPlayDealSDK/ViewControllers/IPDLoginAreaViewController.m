//
//  IPDLoginAreaViewController.m
//  SDKSample
//
//  Created by Saman Kumara on 7/14/16.
//  Copyright © 2016 Saman Kumara. All rights reserved.
//

#import "IPDLoginAreaViewController.h"
#import "IPlayDealManager.h"
#import "AppUserDefaults.h"

#define UIColorFromRGBIPD(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface IPDLoginAreaViewController () {
    BOOL isLoginView;
}
@property (weak, nonatomic) IBOutlet UITableViewCell *userNameCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *passwordCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *confirmPasswordCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *buttonCell;

@property (weak, nonatomic) IBOutlet UITextField *textField_1;
@property (weak, nonatomic) IBOutlet UITextField *textField_2;
@property (weak, nonatomic) IBOutlet UITextField *textField_3;
@property (weak, nonatomic) IBOutlet UITextField *textField_4;
@property (weak, nonatomic) IBOutlet UITextField *textField_5;

@property (weak, nonatomic) IBOutlet UIView *menuSignUpHeader;
@property (weak, nonatomic) IBOutlet UIView *menuSignInHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnActionButton;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
@property (weak, nonatomic) IBOutlet UIImageView *passwordTV;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *processActivityIndicator;
@property (weak, nonatomic) IBOutlet UIView *activityIndicatorView;

@end

@implementation IPDLoginAreaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isLoginView = YES;
    self.activityIndicatorView.hidden = YES;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
            return 100.0;
            break;
        case 1:
            if (isLoginView) {
                [_textField_1 setPlaceholder:@"User Name"];
            }else{
                [_textField_1 setPlaceholder:@"First Name"];
            }
            break;
        case 2:
            if (isLoginView) {
                [_textField_2 setPlaceholder:@"Password"];
                _textField_2.secureTextEntry = YES;
                _passwordTV.image = [UIImage imageNamed:@"password.png"];
            }else{
                [_textField_2 setPlaceholder:@"Last Name"];
                _textField_2.secureTextEntry = NO;
                _passwordTV.image = [UIImage imageNamed:@"username.png"];
            }
            break;
        case 3:
            if (isLoginView) {
                return 0.0;
            }
            break;
        case 4:
            if (isLoginView) {
                return 0.0;
            }
            break;
        case 5:
            if (isLoginView) {
                return 0.0;
            }
            break;
        case 6:
            if (isLoginView) {
                [self.btnActionButton setTitle:@"SIGN IN" forState:UIControlStateNormal];
                self.menuSignInHeader.backgroundColor = UIColorFromRGBIPD(0x3C93C9);
                self.menuSignUpHeader.backgroundColor = UIColorFromRGBIPD(0xE6E6E6);
                self.btnForgotPassword.hidden = NO;
            }else{
                [self.btnActionButton setTitle:@"SIGN UP" forState:UIControlStateNormal];
                 self.menuSignUpHeader.backgroundColor = UIColorFromRGBIPD(0x3C93C9);
                 self.menuSignInHeader.backgroundColor = UIColorFromRGBIPD(0xE6E6E6);
                self.btnForgotPassword.hidden = NO;
            }
            return 130.0;
            break;
        
        default:
            break;
    }
    return 47.0;
}

- (IBAction)tapOnButton:(UIButton *)sender {
    
    if ([sender.currentTitle.lowercaseString isEqualToString:@"sign in"]) {
        if (self.textField_1.text.length != 0 && self.textField_2.text.length != 0) {
            NSString * email = self.textField_1.text;
            if ([self validEmail:email]) {
                 [self loginAction];
            }else{
                UIAlertView *aa = [[UIAlertView alloc]initWithTitle:@"" message:@"Email address invalid." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [aa show];
            }
        }else{
            UIAlertView *aa = [[UIAlertView alloc]initWithTitle:@"" message:@"Please fill all fields." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [aa show];
        }
    }else {
        NSString * firstName = self.textField_1.text;
        NSString * lastName = self.textField_2.text;
        NSString * email = self.textField_3.text;
        NSString * password = self.textField_4.text;
        NSString * confirm_password = self.textField_4.text;
        
        if (firstName.length >0 && lastName.length >0 && email.length >0 && password.length >0 && confirm_password.length >0) {
            if ([self validEmail:email]) {
                [self signUpAction];
            }else{
                UIAlertView *aa = [[UIAlertView alloc]initWithTitle:@"" message:@"Email address invalid." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [aa show];
            }
        }else{
            UIAlertView *aa = [[UIAlertView alloc]initWithTitle:@"" message:@"Please fill all fields." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [aa show];
        }
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self clearData];
}
#pragma mark -
#pragma mark Validations
-(BOOL)validEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(BOOL)validateSignUp :(NSString * )field{
    if (field.length > 0) {
        return YES;
    }
    return NO;
}
-(void)loginAction{
    
    self.activityIndicatorView.hidden = NO;
    NSString * userName = self.textField_1.text;
    NSString * password = self.textField_2.text;
    
    [[IPDWebserviceApi sharedManager] setDelegate:self];
    [[IPDWebserviceApi sharedManager] Login:userName :password];

}
-(void)didReceiveLogininfo:(id)respond Status:(BOOL)isSuccess{
    
    dispatch_async(dispatch_get_main_queue(), ^{self.activityIndicatorView.hidden = YES;});
    
    if (isSuccess) {
        if ([respond objectForKey:@"oauth" ]) {
            [AppUserDefaults setAccessToken:[[respond objectForKey:@"oauth"] objectForKey:@"access_token"]];
            [AppUserDefaults setRefreshToken:[[respond objectForKey:@"oauth"] objectForKey:@"refresh_token"]];
             [self dismissViewControllerAnimated:YES completion:nil];
        }else {
            [self showAlert:@"" message:[[respond objectForKey:@"status"] objectForKey:@"message"]];
        }
    }else{
        [self showAlert:@"" message:[[respond objectForKey:@"status"] objectForKey:@"message"]];
    }
}

-(void)signUpAction{

    self.activityIndicatorView.hidden = NO;
    NSString * firstName = self.textField_1.text;
    NSString * lastName = self.textField_2.text;
    NSString * email = self.textField_3.text;
    NSString * password = self.textField_4.text;
    
    [[IPDWebserviceApi sharedManager] setDelegate:self];
    [[IPDWebserviceApi sharedManager] SignUp:firstName
                                     LatName:lastName
                                       Email:email
                                    Password:password];
}
-(void)didReceiveSignUpinfo:(id)respond Status:(BOOL)isSuccess{
    
    dispatch_async(dispatch_get_main_queue(), ^{self.activityIndicatorView.hidden = YES;});
    if (isSuccess) {
        [[NSUserDefaults standardUserDefaults]setObject:[[respond objectForKey:@"status" ]objectForKey:@"oauth"] forKey:@"token"];
        if ([NSThread isMainThread]) {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            });
        }
    }else{
        [self showAlert:@"" message:[[respond objectForKey:@"status"] objectForKey:@"message"]];
    }
}
- (IBAction)tapMenuButton:(UIButton *)sender {
    if (sender.tag == 0) {
        isLoginView = YES;
    }else {
        isLoginView = NO;
    }
    [UIView transitionWithView: self.tableView
                      duration: 0.35f
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void)
    {
         [self.tableView reloadData];
    }
                    completion: nil];
    [self clearData];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_textField_1 resignFirstResponder];
    [_textField_2 resignFirstResponder];
    [_textField_3 resignFirstResponder];
    [_textField_4 resignFirstResponder];
    [_textField_5 resignFirstResponder];
}
-(void)clearData{
    _textField_1.text = @"";
    _textField_2.text = @"";
    _textField_3.text = @"";
    _textField_4.text = @"";
    _textField_5.text = @"";
    
    [_textField_1 resignFirstResponder];
    [_textField_2 resignFirstResponder];
    [_textField_3 resignFirstResponder];
    [_textField_4 resignFirstResponder];
    [_textField_5 resignFirstResponder];
}
- (IBAction)btnSkipClicks:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)activityIndicatorShow :(BOOL)isShow{
    if (isShow) {
        self.processActivityIndicator.hidden = NO;
        [self.processActivityIndicator startAnimating];
    }else{
        [self.processActivityIndicator stopAnimating];
        self.processActivityIndicator.hidden = YES;
    }
}
-(void)showAlert :(NSString *)title message:(NSString *)msg{
    
    NSString * message =msg;
    if (message == nil)message = NSLocalizedString(@"unrecognized_respond", nil);
    
    UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:title
                                                                               message: message
                                                                        preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [myAlertController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [myAlertController addAction: ok];
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:myAlertController animated:YES completion:nil];
    });
}
@end
