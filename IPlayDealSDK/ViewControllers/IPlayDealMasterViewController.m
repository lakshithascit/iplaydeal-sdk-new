//
//  IPlayDealMasterViewController.m
//  TestSDK
//
//  Created by SCIT on 8/24/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import "IPlayDealMasterViewController.h"
#import "AppUserDefaults.h"
#import "Reachability.h"

@interface IPlayDealMasterViewController ()

@end

@implementation IPlayDealMasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

@end
