//
//  IPlayDealManager.h
//  IPlayDealSDK
//
//  Created by Saman Kumara on 7/5/16.
//  Copyright © 2016 iplayDeal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "IPDWebserviceApi.h"


@interface IPlayDealManager : NSObject<IPDWebservicedelegate>
@property (nonnull, strong) NSString                *appKey, *appSecret, *appVersion;

NS_ASSUME_NONNULL_BEGIN
+ (id)sharedManagerWithAppKey:( nullable NSString *)key secret:(nullable NSString *)secret appVersion:(nullable NSString *)version  ;
- (void)showLoginDialog:(UIViewController *)viewController;
- (void)signout;
- (void)sendDetails:(NSString *)params;
-(void)sendOfflineDetails:(NSString *)params;
+ (id)sharedManager;
- (BOOL)isLoggedIn;
NS_ASSUME_NONNULL_END
@end

//@interface IPDReqeestHandler : NSObject
//NS_ASSUME_NONNULL_BEGIN
//+(NSMutableURLRequest *)getRequestFromURL:(NSString *)url;
//NS_ASSUME_NONNULL_END

//@end
