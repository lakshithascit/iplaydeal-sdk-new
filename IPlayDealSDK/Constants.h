//
//  Constants.h
//  Customer
//
//  Created by Lakshitha on 26/11/2014.
//  Copyright (c) 2014 User. All rights reserved.
//  Final Vertion 2016/Aug/18
//IN QA version 1.0

#ifndef Cashier_Constants_h
#define Cashier_Constants_h


#define kOFFSET_FOR_KEYBOARD 120.0

//RegX
#define CONNECTION_SERVER                              @"www.apple.com"
#define SDKBUNDLE_ID                                   @"com.softcodeit.sdk.IPlayDealSDK";

//RegX
#define REGEX_USER_NAME_LIMIT                          @"^.{1,100}$"
#define REGEX_ADDRESS_LIMIT                            @"^.{10,100}$"
#define REGEX_USER_NAME                                @"[A-Za-z0-9 ]*"
#define REGEX_EMAIL                                    @"[A-Z0-9a-z._%+-]{1,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT                           @"^.{1,20}$"
#define REGEX_PASSWORD                                 @"[A-Za-z0-9]{6,20}"
#define REGEX_PHONE_DEFAULT                            @"[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"

//New Server
#define SERVICEURL                                      @"http://sandboxapi.qa.iplaydeal.com/v1/"  //  WebService URL - SadBox

//#define SERVICEURL                                      @"http://sandboxapi.dev.iplaydeal.com/v1/"  //  WebService URL - SadBox

//#define SERVICEURL                                      @"http://52.40.65.38/api/"  //  WebService URL - Test





#endif
